package Actors

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.{AbstractBehavior, ActorContext, Behaviors}
import io.zeebe.client.api.command.PublishMessageCommandStep1
import io.zeebe.client.api.response.ActivatedJob
import io.zeebe.client.api.worker.JobClient

import scala.jdk.CollectionConverters._
import scala.util.Random

abstract class PathGeneratorMsg

object PathGenerator {
  final case class Generate(distance: String) extends PathGeneratorMsg
  def apply(client: JobClient, job: ActivatedJob, messageBuilder: PublishMessageCommandStep1): Behavior[PathGeneratorMsg] =
    Behaviors.setup(context => new PathGenerator(context, client, job, messageBuilder))
}

class PathGenerator (context: ActorContext[PathGeneratorMsg], client: JobClient, job: ActivatedJob, messageBuilder: PublishMessageCommandStep1)
  extends AbstractBehavior[PathGeneratorMsg](context){
  override def onMessage(msg: PathGeneratorMsg): Behavior[PathGeneratorMsg] = {
    msg match {
      case PathGenerator.Generate(_) =>
        val distance: Float = Random.between(0f, 3f)
        println(s"generated optimal distance $distance")
        client.newCompleteCommand(job.getKey).
          variables(Map("distance" -> distance, "workflowInstanceKey" -> job.getWorkflowInstanceKey).asJava)
          .send()
          .join()
        println("completed job")
        messageBuilder.messageName("path-generated").
          correlationKey(job.getVariablesAsMap.get("deliveryId").asInstanceOf[Integer].toString).send()
        println(s"published message with correlation key ${job.getVariablesAsMap.get("deliveryId")}")
        Behaviors.stopped(()=>{})
    }
  }
}