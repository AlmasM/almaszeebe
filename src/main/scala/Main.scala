import io.zeebe.client.ZeebeClient


object Main extends App {
  val zeebeClient: ZeebeClient = ZeebeClient.newClientBuilder().brokerContactPoint("localhost:26500")
    .usePlaintext().build()
  zeebeClient.newWorker().jobType("generate-optimal").handler{
    new ServiceWorker(zeebeClient)
  }.open()
  zeebeClient.newWorker().jobType("init-free").handler{
    new ServiceWorker(zeebeClient)
  }.open()
  zeebeClient.newWorker().jobType("init-standart").handler{
    new ServiceWorker(zeebeClient)
  }.open()
  zeebeClient.newWorker().jobType("init-express").handler{
    new ServiceWorker(zeebeClient)
  }.open()
}