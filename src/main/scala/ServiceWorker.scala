import Actors.{DeliveryType, PathGenerator}
import akka.actor.typed.ActorSystem
import io.zeebe.client.ZeebeClient
import io.zeebe.client.api.response.ActivatedJob
import io.zeebe.client.api.worker.{JobClient, JobHandler}

class ServiceWorker (zeebeClient: ZeebeClient) extends JobHandler{
  override def handle(client: JobClient, job: ActivatedJob): Unit = {
    job.getType match {
      case "generate-optimal" =>
        ActorSystem(PathGenerator(client, job, zeebeClient.newPublishMessageCommand()), "generator") ! PathGenerator.Generate("12")
      case "init-free" =>
        ActorSystem(DeliveryType.Free(client, job), "free") ! DeliveryType.DeliverPackage("Almas")
      case "init-standart" =>
        ActorSystem(DeliveryType.Standart(client, job), "standart") ! DeliveryType.DeliverPackage("Daulet")
      case "init-express" =>
        ActorSystem(DeliveryType.Express(client, job), "express") ! DeliveryType.DeliverPackage("Vasya")
    }
  }
}